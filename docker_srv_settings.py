import os
import base64

SECRET_KEY = base64.b64encode(os.urandom(256))

DEBUG = False

ALLOWED_HOSTS = ['*', 'forum.f-droid.org', 'forum.f-droid.org:8000']

DATABASES = {
    'default': {
	'ENGINE': 'django.db.backends.sqlite3',
	'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

HAYSTACK_CONNECTIONS = {
    'default': {
	'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
	'PATH': os.path.join(BASE_DIR, 'whoosh_index'),
    },
} 

STATIC_ROOT = os.path.join(BASE_DIR, 'static_files')
STATIC_URL = '/static/'
