# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [Unreleased]
_no changes yet_

## [0.3.15] - 2021-02-10
### Added
- meta viewport for better mobile support

## [0.3.14] - 2020-10-18
### Changed
- use fixed dependencies from now on

### Fixed
- upgrade Docker image to buster, because the version of pip in stretch is not
  functioning anymore

## [0.3.13] - 2020-08-16
### Added
- support for open search description

## [0.3.12] - 2019-12-21
### Added
- new json api: api/config\_info

## [0.3.11] - 2019-09-02
### Added
- improve search results: find super-script digits (eg. davx5 -> DAVx⁵)

## [0.3.10] - 2019-06-24
### Changed
- update to standard F-Droid logo

## [0.3.9] - 2019-05-27
### Added
- improve search results: split camel case words (eg. nlp -> UnifiedNlp)
- improve search results: colapse dashes (eg. k9 -> k-9)

## [0.3.8] - 2019-05-25
### Changed
- use EdgeNgramField for finding partial names

## [0.3.7] - 2019-05-25
### Changed
- drastically reduce index size: only index name and summary

## [0.3.6] - 2019-04-30
### Changed
- start dockerized app into ram-disk when available

## [0.3.5] - 2019-04-30
### Added
- autofocus search input text when search query is empty
- support for loading icons from mirror

### Changed
- display localized icons when available

## [0.3.4] - 2019-04-27
### Changed
- updated CI deployment deploy 'production' branch instead of 'master'

## [0.3.3] - 2019-04-24
### Added
- mapping for ambiguous zh language codes

### Fixed
- language code based language selection
- filter search result links for language codes

## [0.3.2] - 2019-02-16
### Added
- added changelog

### Changed
- upgraded to Django 2

## [0.3.1] - 2019-02-08
### Added
- explicit static file folder settings

### Fixed
- unescape html encoded app titles, summaries etc.
- display translated app titles in search results
- do subsequent search queries without changing selected language to english
- set template doc-type to html5

### Deprecated
- curl will be removed from the docker container next release

### Removed
- removed hack for fixing static links from legacy deployment

### Security
- moved CSS embedded Search into a static file (for allowing more strict CSP)
- configured production django-settings for docker container

[Unreleasead]: https://gitlab.com/fdroid/fdroid-website-search/compare/0.3.15...master
[0.3.15]: https://gitlab.com/fdroid/fdroid-website-search/compare/0.3.14...0.3.15
[0.3.14]: https://gitlab.com/fdroid/fdroid-website-search/compare/0.3.13...0.3.14
[0.3.13]: https://gitlab.com/fdroid/fdroid-website-search/compare/0.3.12...0.3.13
[0.3.12]: https://gitlab.com/fdroid/fdroid-website-search/compare/0.3.11...0.3.12
[0.3.11]: https://gitlab.com/fdroid/fdroid-website-search/compare/0.3.10...0.3.11
[0.3.10]: https://gitlab.com/fdroid/fdroid-website-search/compare/0.3.9...0.3.10
[0.3.9]: https://gitlab.com/fdroid/fdroid-website-search/compare/0.3.8...0.3.9
[0.3.8]: https://gitlab.com/fdroid/fdroid-website-search/compare/0.3.7...0.3.8
[0.3.7]: https://gitlab.com/fdroid/fdroid-website-search/compare/0.3.6...0.3.7
[0.3.6]: https://gitlab.com/fdroid/fdroid-website-search/compare/0.3.5...0.3.6
[0.3.5]: https://gitlab.com/fdroid/fdroid-website-search/compare/0.3.4...0.3.5
[0.3.4]: https://gitlab.com/fdroid/fdroid-website-search/compare/0.3.3...0.3.4
[0.3.3]: https://gitlab.com/fdroid/fdroid-website-search/compare/0.3.2...0.3.3
[0.3.2]: https://gitlab.com/fdroid/fdroid-website-search/compare/0.3.1...0.3.2
[0.3.1]: https://gitlab.com/fdroid/fdroid-website-search/compare/0.3...0.3.1
[0.3]: https://gitlab.com/fdroid/fdroid-website-search/tags/0.3
