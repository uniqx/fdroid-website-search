#!/usr/bin/env python3
#
# fdroidfetchindex.py - part of fdroid-website-search django application
# Copyright (C) 2017-2019 Michael Pöhn <michael.poehn@fsfe.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ObjectDoesNotExist

from fdroid_website_search.templatetags import fdroid_website_search_tags

from fdroid_website_search.models import Language
from fdroid_website_search.models import FDroidSite
from fdroid_website_search.models import App
from fdroid_website_search.models import AppI18n
from fdroid_website_search.models import Category
from fdroid_website_search.models import AntiFeature
from fdroid_website_search.models import License

from collections import defaultdict

import os
import json
import html
import zipfile
import tempfile
import urllib.request
import datetime

class Command(BaseCommand):
    def handle(self, *args, **options):

        def get_or_create_lang(name):
            try:
                return Language.objects.get(name=name)
            except ObjectDoesNotExist:
                l = Language()
                l.name = name
                l.save()
                return l

        def get_or_create_site(repo, site, iconMirror):
            try:
                return FDroidSite.objects.get(repoUrl=repo, siteUrl=site)
            except ObjectDoesNotExist:
                s = FDroidSite()
                s.repoUrl = repo
                s.siteUrl = site
                s.iconMirrorUrl = iconMirror
                s.save()
                return s

        def get_or_create_category(name):
            try:
                return Category.objects.get(name=name)
            except ObjectDoesNotExist:
                c = Category()
                c.name = name
                c.save()
                return c

        def get_or_create_antifeature(name):
            try:
                return AntiFeature.objects.get(name=name)
            except ObjectDoesNotExist:
                af = AntiFeature()
                af.name = name
                af.save()
                return af

        def get_or_create_license(name):
            try:
                return License.objects.get(name=name)
            except ObjectDoesNotExist:
                l = License()
                l.name = name
                l.save()
                return l

        for fdroid_site_info in fdroid_website_search_tags.get_all_repos():
            fdroid_site = get_or_create_site(fdroid_site_info['repo'],
                                             fdroid_site_info['site'],
                                             fdroid_site_info['icon-mirror'])

            with tempfile.TemporaryDirectory() as tmpdir:
                jarpath = os.path.join(tmpdir, 'index-v1.jar')
                index_url = fdroid_site.repoUrl + '/index-v1.jar'
                print("fetching '{}' ...".format(index_url))
                with urllib.request.urlopen(index_url) as i:
                    with open(jarpath, 'wb') as o:
                        o.write(i.read())
                with zipfile.ZipFile(jarpath) as z:
                    index = json.loads(z.read('index-v1.json').decode('utf-8'))

                fdroid_site.lastUpdated = datetime.datetime.now(datetime.timezone.utc)
                fdroid_site.repoTimestamp = datetime.datetime.fromtimestamp(index['repo']['timestamp']/1000., datetime.timezone.utc)
                fdroid_site.save()

                print("updating database ...")
                for app in index['apps']:
                    try:
                        a = App.objects.get(packageName=app['packageName'])
                    except ObjectDoesNotExist:
                        a = App()
                    a.name = html.unescape(app.get('name', ''))
                    a.packageName = app.get('packageName', '')
                    a.summary = html.unescape(app.get('summary', ''))
                    a.description = html.unescape(app.get('description', ''))
                    a.whatsNew = html.unescape(app.get('whatsNew', ''))

                    a.authorName = html.unescape(app.get('authorName', ''))
                    a.authorEmail = html.unescape(app.get('authorEmail', ''))

                    a.license = get_or_create_license(app.get('license', None))

                    a.webSite = app.get('webSite', '')
                    a.sourceCode = app.get('sourceCode', '')
                    a.issueTracker = app.get('issueTracker', '')
                    a.changelog = app.get('changelog', '')

                    a.donate = app.get('donate', '')
                    a.bitcoin = app.get('bitcoin', '')
                    a.litecoin = app.get('litecoin', '')
                    a.flattrID = app.get('flattrID', '')
                    a.liberapayID = app.get('liberapayID', '')

                    a.added = datetime.datetime.fromtimestamp(int(app.get('added', 0))/1000., datetime.timezone.utc)
                    a.lastUpdated = datetime.datetime.fromtimestamp(int(app.get('lastUpdated', 0))/1000., datetime.timezone.utc)

                    a.icon = app.get('icon', '')
                    a.site = fdroid_site
                    a.save()

                    a.categories.clear()
                    for category in app.get('categories', []):
                        a.categories.add(get_or_create_category(html.unescape(category)))
                    a.antiFeatures.clear()
                    for antiFeature in app.get('antiFeatures', []):
                        a.antiFeatures.add(get_or_create_antifeature(html.unescape(antiFeature)))

                    for langName, loc in app.get('localized', {}).items():
                        l = get_or_create_lang(fdroid_website_search_tags._map_lang(langName))
                        try:
                            aI18n = AppI18n.objects.get(entry=a, lang=l)
                        except ObjectDoesNotExist:
                            aI18n = AppI18n()
                        aI18n.name = html.unescape(loc.get('name', ''))
                        aI18n.summary = html.unescape(loc.get('summary', ''))
                        aI18n.description = html.unescape(loc.get('description', ''))
                        aI18n.whatsNew = html.unescape(loc.get('whatsNew', ''))
                        localizedIcon = html.unescape(loc.get('icon', ''))
                        if localizedIcon:
                            aI18n.icon = '/' + langName + '/' + localizedIcon
                        else:
                            aI18n.icon = ''
                        aI18n.entry = a
                        aI18n.lang = l
                        aI18n.save()

            print("Parsed infos of {} Apps and stored them in the local database.".format(len(index['apps'])))
        print("There are infos about {} Apps stored in the local database now.".format(len(App.objects.all())))
