# from haystack.views import SearchView
from haystack.generic_views import SearchView
from django.http import JsonResponse

from .forms import FDroidSearchForm
from .models import Language, FDroidSite

from .templatetags import fdroid_website_search_tags


class FDroidSearchView(SearchView):

    form_class = FDroidSearchForm

    def get_context_data(self, **kwargs):
        context = super(FDroidSearchView, self).get_context_data(**kwargs)

        lang = fdroid_website_search_tags._map_lang(self.request.GET.get('lang', ''))
        if lang:
            try:
                Language.objects.get(name=lang)
                context['lang'] = lang
            except Language.DoesNotExist:
                pass

        return context


def config_info(req):

    d = {}

    d['repos'] = []
    for site in FDroidSite.objects.all():
        d['repos'].append({'last_updated': site.lastUpdated.isoformat(),
                           'repo_timestamp': site.repoTimestamp.isoformat(),
                           'site_url': site.siteUrl,
                           'repo_url': site.repoUrl,
                           'icon_mirror_url': site.iconMirrorUrl})

    return JsonResponse(d)
